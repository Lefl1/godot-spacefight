extends WAT.Test

var root: MapSceneRoot
var player: PlayerShip
var mission: Mission
var stage: MissionStage
var objective: MissionObjective


func title() -> String:
	return "Test Mission Objectives"


func pre() -> void:
	root = MapSceneRoot.new()
	print("[TEST] Ignore next messages about nodes not being found.")

	mission = Mission.new()
	stage = MissionStage.new()
	mission.add_child(stage, true)
	objective = MissionObjectiveDummy.new()
	objective.description = "TESTINGTESTINGATTENTIONPLEASE"
	stage.add_child(objective)

	mission.add_child(MissionStageEnd.new())
	mission.add_child(MissionStageFail.new())
	root.add_child(mission)

	player = load("res://actors/ipc/player_ship/player.tscn").instance()
	root.add_child(player)
	add_child(root)
	player.owner = root
	mission.owner = root
	root.mission = mission
	root.local_player = player


func post():
	root.free()


func test_objective_kill_specific() -> void:
	describe("KillSpecific")
	objective.queue_free()

	objective = MissionObjectiveKillSpecific.new()
	var actor := Actor.new()
	root.add_child(actor)
	objective.objective_path = actor.get_path()
	stage.add_child(objective)

	watch(objective, "objective_completed")
	actor.die()
	_test_objective_completion()


func test_objective_kill_children() -> void:
	describe("KillChildren")
	objective.queue_free()

	objective = MissionObjectiveKillChildren.new()
	var actors_root = Spatial.new()
	for i in range(0, 4):
		actors_root.add_child(Actor.new())
	root.add_child(actors_root)
	objective.objective_path = actors_root.get_path()
	stage.add_child(objective)

	watch(objective, "objective_completed")
	for child in actors_root.get_children():
		child.die()
	_test_objective_completion()


func test_objective_protect() -> void:
	describe("Protect")
	objective.queue_free()

	objective = MissionObjectiveProtect.new()
	var actor = Actor.new()
	root.add_child(actor)
	objective.objective_path = actor.get_path()
	stage.add_child(objective)

	watch(objective, "objective_failed")
	actor.die()
	_test_objective_failure()


func test_objective_reach() -> void:
	describe("Reach")
	objective.queue_free()

	objective = MissionObjectiveReach.new()
	var waypoint := Spatial.new()

	objective.margin = 5
	root.add_child(waypoint)
	waypoint.global_transform.origin.x = 50
	objective.objective_path = waypoint.get_path()
	objective.player = player

	stage.add_child(objective)

	watch(objective, "objective_completed")
	player.global_transform.origin.x = 46
	objective._check_distance()
	_test_objective_completion()


func test_objective_wait() -> void:
	describe("Wait")
	objective.queue_free()
	objective = MissionObjectiveWait.new()
	objective.WAIT_TIME = 5.0
	stage.add_child(objective)
	watch(objective, "objective_completed")
	var i := 0
	while true:
		objective._process(1.0)
		if i == 6:
			break
		i += 1
	_test_objective_completion()


func _test_objective_completion() -> void:
	asserts.signal_was_emitted_with_arguments(objective, "objective_completed", [objective], "Objective completion signal caught")
	asserts.is_true(objective.completed, "'objective.completed' is true")
	asserts.is_false(objective.active, "Objective is not active")


func _test_objective_failure() -> void:
	asserts.signal_was_emitted_with_arguments(objective, "objective_failed", [objective], "Objective completion signal caught")
	asserts.is_true(objective.failed, "'objective.failed' is true")
	asserts.is_false(objective.active, "Objective is not active")
