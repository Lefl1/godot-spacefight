class_name ShipHUD
extends CanvasLayer


export (NodePath) var health_bar_path: NodePath
onready var health_bar := (get_node_or_null(health_bar_path) as ProgressBar)
export (NodePath) var shield_bar_path: NodePath
onready var shield_bar := (get_node_or_null(shield_bar_path) as ProgressBar)


func update_health_bars(percent_health, percent_shield) -> void:
	health_bar.value = percent_health
	shield_bar.value = percent_shield
	(health_bar.get_node("Label") as Label).text = str(int(percent_health)) + "%"
	(shield_bar.get_node("Label") as Label).text = str(int(percent_shield)) + "%"
