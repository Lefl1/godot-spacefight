extends Control


var main_menu


func _ready() -> void:
	main_menu = load("res://scenes/gui/menus/main_menu/main_menu.tscn")


func _on_fullscreen_toggled(button_pressed) -> void:
	OS.set_window_fullscreen(button_pressed)
	ConfigHandler.set_config("display", "fullscreen", button_pressed)


func _on_vsync_toggled(button_pressed) -> void:
	OS.set_use_vsync(button_pressed)
	ConfigHandler.set_config("display", "vsync", button_pressed)


func _on_apply_name_pressed() -> void:
# warning-ignore:unsafe_method_access
	var mname = $"multiplayer/options/Label/TextEdit".get_text()
	MultiplayerHandler.local_info["name"] = mname
	ConfigHandler.set_config("multiplayer", "name", mname)


func _on_back_pressed() -> void:
	get_tree().change_scene_to(main_menu)


func _on_save_pressed() -> void:
	ConfigHandler.save()
	get_tree().change_scene_to(main_menu)
