extends Control

#const singleplayer_scene = preload("res://scenes/maps/demoscene/demoscene.tscn")
const multiplayer_scene = preload("res://scenes/gui/menus/lobby/lobby.tscn")
const options_scene = preload("res://scenes/gui/menus/main_menu/options.tscn")


#func _on_singleplayer_pressed() -> void:
#	get_tree().change_scene_to(singleplayer_scene)

func _on_multiplayer_pressed() -> void:
	get_tree().change_scene_to(multiplayer_scene)

func _on_options_pressed() -> void:
	get_tree().change_scene_to(options_scene)
