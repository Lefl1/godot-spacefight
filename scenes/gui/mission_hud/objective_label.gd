class_name ObjectiveLabel
extends Label

signal faded_in

func fadeout(_objective: MissionObjective) -> void:
	($AnimationPlayer as AnimationPlayer).play("fadeout")

func fadein() -> void:
	($AnimationPlayer as AnimationPlayer).play("fadein")

func next_fadein() -> void:
	emit_signal("faded_in")
