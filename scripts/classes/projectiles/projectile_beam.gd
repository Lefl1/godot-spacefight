extends Projectile
class_name ProjectileBeam
# Projectile class for beams


const BEAM_PARTICLE_VELOCITY := 400.0
var BEAM_MAX_LENGTH := 100.0
var beam_particles: Particles


func _init(properties: Dictionary, source: Spatial).(properties, source) -> void:
	# We're a beam, no matter what we're told, we're not moving.
	linear_velocity = 0.0
	# Save the damage per second. We need to save the damager per frame in the damage field.
	damage = damage.duplicate() # Needed, otherwise other instances reduce the damage
	damage["damage_per_second"] = damage["damage"]
	# Beams cannot explode.
	is_explosive = false
	BEAM_MAX_LENGTH = properties["max_beam_distance"]
	beam_particles = model.get_node_or_null("BeamParticles")
	cast_to = Vector3(0, 0, -BEAM_MAX_LENGTH)



func collide(delta: float) -> void:
	var beam_length := BEAM_MAX_LENGTH
	var collider: Spatial
	if is_colliding() and not is_dying:
		collider = get_collider()
		beam_length = (get_collision_point() - global_transform.origin).length()
	if is_instance_valid(collider) and not is_dummy:
		if collider is Actor:
			damage["damage"] = damage["damage_per_second"] * delta
			(collider as Actor).recieve_damage(damage, source)

	scale_beam(beam_length)


func scale_beam(distance: float) -> void:
	beam_particles.lifetime = distance / BEAM_PARTICLE_VELOCITY
