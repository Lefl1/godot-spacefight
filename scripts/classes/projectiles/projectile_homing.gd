extends Projectile
class_name ProjectileHoming
# Projectile class for all homing projectiles, most notably missiles.


var angular_velocity := 0.0
var target_path: NodePath
var target: Spatial


func _init(properties: Dictionary, source: Spatial).(properties, source) -> void:
	angular_velocity = properties["angular_velocity"]


func _ready() -> void:
	if not target_path.is_empty():
		target = get_node(target_path)


func _process(delta) -> void:
	if is_instance_valid(target):
		track_target(target, delta)


func track_target(entity: Spatial, delta: float) -> void:
	var target_position := entity.global_transform.origin
	var new_tform := global_transform.looking_at(target_position, global_transform.basis.y)
	new_tform = global_transform.interpolate_with(new_tform, angular_velocity * delta)
	global_transform = new_tform
