class_name Mission, "res://icons/mission.svg"
extends Node

signal mission_initialized
signal changed_stage
export (String) var description: String
var player: PlayerShip
onready var stages := get_children()
var current_stage: MissionStage
puppet var current_stage_idx := 0 setget set_current_stage
var players_alive := []


func _ready() -> void:
	for stage in stages:
		if not stage is MissionStageFail:
			stage.connect("stage_completed", self, "_stage_completed", [], CONNECT_ONESHOT)
			stage.connect("stage_failed", self, "_stage_failed", [], CONNECT_ONESHOT)

	if stages.size() == 0:
		print("[INFO] No stages in mission.")
		return

	current_stage = (stages[0] as MissionStage)
	# TODO Check if the last two stages are failure and end stages
	if not stages[stages.size()-2] is MissionStageEnd:
		print("[WARNING] Mission has no end stage! Add an 'MissionStageEnd' object as the second last child.'")
	if not stages[stages.size()-1] is MissionStageFail:
		print("[WARNING] Mission has no fail stage! Add an 'MissionStageFail' object as the last child.")


func init_mission(local_player: PlayerShip, players_node: Spatial = null) -> void:
	if MultiplayerHandler.is_singleplayer() or not is_network_master():
		player = local_player
		player.connect("has_died", self, "_fail_mission")
		connect("mission_initialized", player.mission_hud, "init")
		connect("changed_stage", player.mission_hud, "_update")
	else:
		assert(players_node, "[ERROR] Mission requires a players_node in multiplayer!")
		for child_player in players_node.get_children():
			players_alive.append(child_player)
			child_player.connect("has_died", self, "_player_died")

	current_stage.active = true
	emit_signal("mission_initialized", self)
	print("[INFO] Initialized Misison.")


func _stage_completed(_stage: MissionStage) -> void:
	# If were master or in singleplayer advance the stage.s
	if MultiplayerHandler.is_singleplayer():
		self.current_stage_idx += 1
	elif is_network_master():
		self.current_stage_idx += 1
		rset("current_stage_idx", current_stage_idx)


func _stage_failed(_stage: MissionStage) -> void:
	_fail_mission()


# This accepts a variable simply to please the has_died signal of the player
# TODO: handle player deaths
func _fail_mission() -> void:
	if MultiplayerHandler.is_singleplayer():
		self.current_stage_idx = stages.size() - 1
	elif is_network_master():
		self.current_stage_idx = stages.size() - 1
		rset("current_stage_idx", current_stage_idx)


func _player_died(dead_player: Actor) -> void:
	if MultiplayerHandler.is_singleplayer():
		_fail_mission()
	else:
		players_alive.erase(dead_player)
		if is_network_master() and players_alive.size() == 0:
			_fail_mission()


puppet func set_current_stage(idx: int) -> void:
	if idx >= stages.size():
		print("[ERROR] Mission tried to activate nonexistent stage %s." % current_stage_idx)
		current_stage_idx = -1
		return
	current_stage = stages[idx]
	current_stage.active = true
	emit_signal("changed_stage")
	current_stage_idx = idx
