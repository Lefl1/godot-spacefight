class_name MissionStageEnd
extends MissionStage


const lobby = preload("res://scenes/ui/multiplayer/hangar_lobby/hangar_lobby.tscn")
const main_menu = preload("res://scenes/gui/menus/main_menu/main_menu.tscn")
onready var canvas_root: Control


func _on_ready() -> void:
	canvas_root = $CanvasLayer/root
	if canvas_root:
		canvas_root.get_node("Button").connect("pressed", self, "_button_pressed", [], CONNECT_ONESHOT)
		if canvas_root.visible:
			canvas_root.visible = false


func set_active(val: bool) -> void:
	if val:
		canvas_root.visible = true
		get_tree().paused = true
	active = val


func _button_pressed() -> void:
	get_tree().paused = false
	if get_tree().has_network_peer():
		get_tree().change_scene_to(lobby)
	else:
		get_tree().change_scene_to(main_menu)
