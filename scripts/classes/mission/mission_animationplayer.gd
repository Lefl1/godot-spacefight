class_name MissionAnimationPlayer, "res://icons/mission_animationplayer.svg"
extends AnimationPlayer
# When there are multiple transition players in the stage they are played in the order
# AnimationPlayer with some extra features for the mission system.
# like they are in the stage.
# Note that transition players do not work as children of MissionObjectives.


enum START_MODES {ON_ACTIVATION, ON_DEACTIVATION}
export (bool) var is_transition := false
export (bool) var pause_while_playing := false
export (START_MODES) var start_mode := START_MODES.ON_ACTIVATION


func _ready() -> void:
	if pause_while_playing:
		pause_mode = Node.PAUSE_MODE_PROCESS
	connect("animation_finished", self, "_animation_finished", [], CONNECT_ONESHOT)


func _play() -> void:
	play(get_animation_list()[0])
	if pause_while_playing:
		get_tree().paused = true


func _animation_finished(_anim: String) -> void:
	if pause_while_playing:
		get_tree().paused = false
