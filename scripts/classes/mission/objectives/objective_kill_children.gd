class_name MissionObjectiveKillChildren
extends MissionObjective


export (NodePath) var objective_path: String
var objective_children: Array
var dead_children := 0



func _ready() -> void:
	objective_children = get_node_or_null(objective_path).get_children()
	for child in objective_children:
		child.connect("has_died", self, "_objective_died")
	progress_string = "%d/%d" % [dead_children, objective_children.size()]


func set_active(val: bool) -> void:
	._set_active(val)
	if val:
		for child in objective_children:
			child.add_to_group("objective")
	active = val


func _objective_died(_actor: Actor) -> void:
	dead_children += 1
	progress_string = "%d/%d" % [dead_children, objective_children.size()]
	update_hud_info()
	if dead_children == objective_children.size():
		_complete_objective()
