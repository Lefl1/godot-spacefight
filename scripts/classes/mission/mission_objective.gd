class_name MissionObjective, "res://icons/mission.svg"
extends Node


signal objective_completed
signal objective_failed
enum OBJECTIVE_TYPES {
	WAIT,
	KILL_SPECIFIC,
	KILL_CHILDREN,
	PROTECT
	}
export var description: String setget ,get_description
export (bool) var append_progress_string := true
var progress_string: String
var active := false setget _set_active
var completed := false
var failed := false

var hud_info: Label


func _ready() -> void:
	call_deferred("set_process", false)


func _process(_delta: float) -> void:
	return


func _set_active(val: bool) -> void:
	set_process(val)

	# If this objective has an MissionAnimation player,
	# play it if's set to be activated on objective start / end.
	var children := get_children()
	for child in children:
		if child is MissionAnimationPlayer:
			if val and child.start_mode == child.START_MODES.ON_ACTIVATION:
				child._play()
			elif not val and child.start_mode == child.START_MODES.ON_DEACTIVATION:
				child._play()

	active = val


func _complete_objective() -> void:
	update_hud_info()
	emit_signal("objective_completed", self)
	self.completed = true
	self.active = false


func _fail_objective() -> void:
	self.failed = true
	self.active = false
	emit_signal("objective_failed", self)
	return


func update_hud_info() -> void:
	if hud_info == null:
		return
	var string := description
	if append_progress_string:
		 string += progress_string
	hud_info.set_text(string)


func get_description() -> String:
	return description + progress_string
