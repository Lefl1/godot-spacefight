extends Actor
class_name AIActor

# AIActor is used by every actor that is acting on its own.

var state: AIState setget _set_state
export (NodePath) var state_package_path
var state_package: Node

export var MAX_TARGET_DIST: int = 0
export (PoolStringArray) var enemy_factions
export (PoolStringArray) var targeted_types
export (float) var maximum_target_distance_difference := 1.25


func _ready() -> void:
	state_package = get_node_or_null(state_package_path)
	if state_package:
		self.state = state_package.get_node("IdleState") as AIState


## Targeting ##
func acquire_target(need_line_of_sight = false) -> Spatial:
	# TODO: Compare both enemies and check which one is better to pursue
	var position := global_transform.origin

	### Normal enemies ###
	var enemies := []
	for faction in enemy_factions:
		enemies += get_tree().get_nodes_in_group(faction)
	var target_entities := []
	var targets_of_interest := []
	if targeted_types.size() == 0:
		targeted_types.append("placeholder")
	for enemy in enemies:
		for type in targeted_types:
			# Check if the enemy is a valid target, is in a targetted group.
			# Optional: check if we have line of sight to it.
			if (is_target_valid(enemy) and (enemy.is_in_group(type) or type == "placeholder")):
				if need_line_of_sight:
					var result := cast_ray(position, enemy.global_transform.origin)
					if result.empty() or result.collider != enemy:
						continue
				# Check if the enemy is an important one and put it in the array if it is.
				if enemy.is_in_group("target_of_interest"):
					targets_of_interest.append(enemy)
				else:
					target_entities.append(enemy)

	var normal_target: Spatial
	if len(target_entities) > 1:
		normal_target = get_closest(position, target_entities)
	elif len(target_entities) == 1:
		normal_target = target_entities[0]

	var target_of_interest: Spatial
	if len(targets_of_interest) > 1:
		target_of_interest = get_closest(position, targets_of_interest)
	elif len(targets_of_interest) == 1:
		target_of_interest = targets_of_interest[0]

	if normal_target and target_of_interest:
		var nt_distance = position.distance_squared_to(normal_target.global_transform.origin)
		var toi_distance = position.distance_squared_to(target_of_interest.global_transform.origin)
		if (toi_distance / nt_distance) < maximum_target_distance_difference:
			return target_of_interest
	elif normal_target and not target_of_interest:
		return normal_target
	elif not normal_target and target_of_interest:
		return target_of_interest
	return null


func _set_state(new_state: AIState) -> void:
	var old_state := state
	if old_state:
		old_state._exit()
		old_state.set_physics_process(false)
	state = new_state
	state._enter(old_state)
	state.set_physics_process(true)


func is_target_valid(target: Spatial) -> bool:
	# Check if a target is valid.
	if not (is_instance_valid(target) and target.is_inside_tree()):
		return false
	elif target.is_in_group("dead") or not target.is_visible_in_tree():
		return false
	# Check if the target is within target range/angle.
	var pos := global_transform.origin
	var dist_to_target := pos.distance_to(target.global_transform.origin)
	if (MAX_TARGET_DIST == 0 or dist_to_target < MAX_TARGET_DIST):
		return true
	return false


func get_closest(position, entity_array: Array) -> Spatial:
	# Get the closest entities inside an entity
	entity_array = cache_distances(position, entity_array)
	var closest: Array = entity_array[0]
	for entity in entity_array:
		if closest[1] > entity[1]:
			if entity[1] < pow(MAX_TARGET_DIST, 2) or MAX_TARGET_DIST == 0:
				closest = entity
	return closest[0]


func cache_distances(position: Vector3, entity_array: Array) -> Array:
	var distance_cache := []
	for entity in entity_array:
		var entity_distance: float = position.distance_squared_to(entity.global_transform.origin)
		distance_cache.append([entity, entity_distance])
	return distance_cache
