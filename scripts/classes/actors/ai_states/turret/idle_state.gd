extends AIState


"""
	State for waiting until a target is ofund
"""


func _enter(_previous_State: AIState) -> void:
	pass


func _physics_process(_delta: float) -> void:
	parent.turn_to_default_transform()
	var temp_target = parent.acquire_target(true)
	if parent.is_target_valid(temp_target):
		parent.target = temp_target
		parent.state = $"../AttackState"

