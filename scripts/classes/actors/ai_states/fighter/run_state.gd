extends AIState


"""
	Follow a target
"""

export var run_distance := 20
var run_position: Vector3


func _enter(_previous_State: AIState) -> void:
	var target_position: Vector3 = parent.calculate_preaim_pos(parent.target, parent.MAX_VELOCITY, -parent.target.get_linear_velocity())
	var target_dir: Vector3 = (target_position + parent.global_transform.origin).normalized() - (parent.global_transform.origin.normalized() + random_vector() * .25).normalized()
	run_position = target_position + target_dir * run_distance


func _physics_process(delta: float) -> void:
	var ray_collisions: Dictionary = parent.get_raycast_collisions()
	parent.move_to(run_position, ray_collisions, delta)
	if run_position.is_equal_approx(parent.global_transform.origin):
		parent.state = $"../HuntState"


func _on_target_locked(_target: Actor) -> void:
	pass


func _on_target_unlocked(_target: Actor) -> void:
	pass


func _on_damage_received(_damage: Dictionary, _source) -> void:
	pass


func _exit() -> void:
	pass
