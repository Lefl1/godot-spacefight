extends Spatial


export var process = true


func _ready() -> void:
	init_processing()


func init_processing() -> void:
	for child in get_children():
		child.set_physics_process(process)
		child.set_process(process)


func toggle_processing() -> void:
	for child in get_children():
		child.set_physics_process(!child.is_physics_processing())
		child.set_process(!child.is_processing())
