extends Spatial

onready var viewport_output := ($CanvasLayer/ViewportOutput as TextureRect)
onready var viewport := ($Viewport as Viewport)


func _ready() -> void:
	get_tree().get_root().connect("size_changed", self, "_on_window_size_change")
	set_size(OS.window_size)


func _on_window_size_change() -> void:
	set_size(OS.window_size)


func set_size(size) -> void:
	viewport.size = size
	viewport_output.rect_size = size
