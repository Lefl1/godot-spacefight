extends Node


signal websocket_authorized
signal received_message
signal received_server_info

var client_info := {"username": null}
var master_server: String
var wsc := WebSocketClient.new()
var session: String


func _ready() -> void:
	master_server = ConfigHandler.get_config("multiplayer", "masterserver")
	call_deferred("load_session_file")


func load_session_file() -> void:
	var f := File.new()
	if f.file_exists("user://session"):
		f.open("user://session", f.READ)
		session = f.get_as_text()
		f.close()
		client_info = parse_json(session)


func save_session_token(email, token) -> void:
	var session_dict := {"email": email, "login_token": token}
	var f := File.new()
	f.open("user://session", File.WRITE)
	f.store_string(to_json(session_dict))
	f.close()


func destroy_session_token() -> void:
	var f := File.new()
	f.open("user://session", File.WRITE)
	f.store_string("{}")
	f.close()


func init_masterserver_connection() -> void:
	if wsc and wsc.get_peer(1).is_connected_to_host():
		return
	printraw("[INFO] Connecting to masterserver...\r")
	var master_server_path = "/ws/"
	if (
		ConfigHandler.force_server or
		ConfigHandler.get_config("server", "start_as_server") or
		OS.has_feature("Server")
		):
		master_server_path += "server"
	else:
		master_server_path += "user"

	var err = wsc.connect_to_url("ws://127.0.0.1:8000" + master_server_path)
	if err != OK:
		print("Got error %s while connecting to masterserver websocket" % err)
	wsc.connect("server_close_request", self, "websocket_server_close")
	yield(WebSocketHandler.wsc, "connection_established")
	print("[INFO] Connected to masterserver.      ")


func login_client(email, password, login_token, save_login=false) -> void:
	WebSocketHandler.send_message({
		"control": "authorize",
		"data": {
			"email": email,
			"password": password,
			"save_login": save_login,
			"login_token": login_token
			}
		})


func login_server(token) -> void:
	WebSocketHandler.send_message({"control": "authorize", "data": {"token": token}})


func handle_websocket() -> void:
	if (
		wsc.get_connection_status() == wsc.CONNECTION_CONNECTING or
		wsc.get_connection_status() == wsc.CONNECTION_CONNECTED
		):
		wsc.poll()

	receive_ws_message()


func receive_ws_message() -> void:
	if not wsc.get_peer(1).is_connected_to_host():
		return
	if wsc.get_peer(1).get_available_packet_count() > 0:
		var packet = wsc.get_peer(1).get_packet()
		var message = packet.get_string_from_utf8()
		message = parse_json(message)
		emit_signal("received_message", message)

		if message["control"] == "notification":
			if message["message"] == "authorized":
				# If we are a server we don't have a username
				if MultiplayerHandler.is_server:
					emit_signal("websocket_authorized")
				else:
					client_info = message["data"]
					MultiplayerHandler.local_info["username"] = client_info["username"]
					if "login_token" in client_info:
						save_session_token(client_info["email"], client_info["login_token"])
					emit_signal("websocket_authorized")

		elif message["control"] == "game_server":
			emit_signal("received_server_info", message["info"])


func send_message(message) -> void:
	if wsc.get_peer(1).is_connected_to_host():
		wsc.get_peer(1).put_packet(to_json(message).to_utf8())


func update_server_info(status) -> void:
	var player_names = []
	for player in MultiplayerHandler.players.values():
		player_names.append(player["username"])

	WebSocketHandler.send_message({
		"control": "submission",
		"message": "server_info",
		"data": {
			"players": player_names,
			"status": status
			}
		})


func close_connection() -> void:
	wsc.disconnect_from_host()


func websocket_server_close(code, reason) -> void:
	print("Server closed connection with reason %s. Code: %s" % [reason, code])


func _process(_delta) -> void:
	if wsc:
		handle_websocket()


func is_ws_connected() -> bool:
	if wsc.get_connection_status() == 0:
		return false
	else:
		return true
