extends Node
# Script for handling all system related shortcuts.
# e.g.: Changing to fullscreen or releasing the mouse


func _ready() -> void:
	set_pause_mode(PAUSE_MODE_PROCESS)


func _input(event) -> void:
	if event is InputEventKey:
		if event.scancode == KEY_ESCAPE:
			if WebSocketHandler.is_ws_connected():
				WebSocketHandler.close_connection()
				yield(WebSocketHandler.wsc, "connection_closed")
			get_tree().quit()

		if event.scancode == KEY_F10 and event.is_pressed():
			OS.set_window_fullscreen(!OS.is_window_fullscreen())

		if event.scancode == KEY_F11 and event.is_pressed():
			if Input.get_mouse_mode() == Input.MOUSE_MODE_VISIBLE:
				Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
			else:
				Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)

