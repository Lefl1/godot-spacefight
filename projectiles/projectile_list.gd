var projectiles := {
	"spec": {
		"id": null,
		"type": Projectile.Type.PROJECTILE,
		"model_resource_path": "res://projectiles/specification/specification.tscn",
		"model_resource": null,
		# Movement and lifetime
		"follow_node": false,
		"linear_velocity": 1.0,
		"max_lifetime": 10.0,
		"angular_velocity": 0.0,	# Homing class only
		"beam_max_distance": 1.0,	# Beam class only

		# Explosive properties.
		# Optional
		"is_explosive": false,
		"damage_distance": 0.0,

		# Proximity detection, only read when explosive is set.
		"has_proximity_detection": false,

		# The actual damage dictionary
		"damage": {
			"type": "",
			"effects": []
		}
	},

	"blue_energy_bullet": {
		"id": null,
		"type": Projectile.Type.PROJECTILE,
		"model_resource_path": "res://projectiles/bullets/blue_energy_bullet/blue_energy_bullet.tscn",
		"model_resource": null,

		"linear_velocity": 90,
		"max_lifetime": 2,

		"damage": {
			"type": "",
			"effects": []
		}
	},

	"red_energy_bullet": {
		"id": null,
		"type": Projectile.Type.PROJECTILE,
		"model_resource_path": "res://projectiles/bullets/blue_energy_bullet/blue_energy_bullet.tscn",
		"model_resource": null,

		"linear_velocity": 60,
		"max_lifetime": 2,

		"damage": {
			"type": "",
			"effects": []
		}
	},

	"red_flak_bullet": {
		"id": null,
		"type": Projectile.Type.PROJECTILE,
		"model_resource_path": "res://projectiles/bullets/red_flak_bullet/red_flak_bullet.tscn",
		"model_resource": null,

		"linear_velocity": 60,
		"max_lifetime": 2,

		"is_explosive": true,
		"damage_distance": 1.5,
		"has_proximity_detection": true,

		"damage": {
			"type": "",
			"effects": []
		}
	},

	"blue_energy_beam": {
		"id": null,
		"type": Projectile.Type.BEAM,
		"model_resource_path": "res://projectiles/beams/blue_energy_beam/blue_energy_beam.tscn",
		"model_resource": null,

		"follow_egress_node": true,
		"linear_velocity": 0.0,
		"max_lifetime": 0.25,
		"angular_velocity": 0.0,
		"max_beam_distance": 100.0,

		"damage": {
			"type": "",
			"effects": []
		}
	},

	"missile": {
		"id": null,
		"type": Projectile.Type.HOMING,
		"model_resource_path": "res://projectiles/missiles/missile/missile.tscn",
		"model_resource": null,

		"linear_velocity": 20.0,
		"max_lifetime": 4.0,
		"angular_velocity": 2.0,

		# Explosive properties.
		# Optional
		"is_explosive": true,
		"damage_distance": 2.0,

		# Proximity detection, only read when explosive is set.
		"has_proximity_detection": true,

		# The actual damage dictionary
		"damage": {
			"type": "",
			"effects": []
		}
	}
}
